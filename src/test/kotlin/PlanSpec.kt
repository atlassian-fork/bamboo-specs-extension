import com.atlassian.bamboo.specs.api.BambooSpec
import com.atlassian.bamboo.specs.api.builders.permission.PermissionType
import com.atlassian.bamboo.specs.api.builders.plan.configuration.AllOtherPluginsConfiguration
import com.atlassian.bamboo.specs.api.builders.requirement.Requirement
import com.atlassian.bamboo.specs.api.model.plan.branches.PlanBranchManagementProperties.NotificationStrategy
import com.atlassian.bamboo.specs.builders.notification.HipChatRecipient
import com.atlassian.bamboo.specs.builders.notification.PlanCompletedNotification
import com.atlassian.bamboo.specs.builders.task.ScriptTask
import com.atlassian.bamboo.specs.builders.task.TestParserTask
import com.atlassian.bamboo.specs.builders.task.VcsCheckoutTask
import com.atlassian.bamboo.specs.builders.trigger.RepositoryPollingTrigger
import com.atlassian.bamboo.specs.extension.*
import com.atlassian.bamboo.specs.model.task.ScriptTaskProperties.Interpreter
import com.atlassian.bamboo.specs.util.BambooServer

@BambooSpec
private object PlanSpec {
    enum class OsType { WINDOWS, LINUX }

    @JvmStatic
    fun main(args: Array<String>) {
        Specs {
            project("PUPPETCI", "Puppet Continuous Deployment", "the Buildeng Puppet Continuous Integration/Deployment project") {
                plan("PUPPETCD", "Puppet CD", "Buildeng Puppet Continuous Integration plan") {
                    pluginConfigurations(AllOtherPluginsConfiguration().configuration(mapOf("custom" to mapOf("planownership.bamboo.plugin.plan.config.ownerOfBuild" to "chebert"))))
                    permissions {
                        loggedInUserPermissions(PermissionType.VIEW, PermissionType.BUILD)
                        anonymousUserPermissionView()
                    }
                    linkedRepositories("Buildeng Puppet Tree")
                    planBranchManagement(BranchManagementType::Vcs, NotificationStrategy.INHERIT) {
                        pattern(".*")
                        cleanup(true, true, 1, 10)
                    }
                    trigger(::RepositoryPollingTrigger, "trigger1")
                    variable("ami.burn.delta", "true")
                    variable("deployment.package", "buildeng-puppet")
                    variable("force.build.all", "false")
                    variable("skip.delta.provision", "false")
                    stage("Create deployment package") {
                        job("VALIDATELINT", "Puppet validate and lint") {
                            pluginConfigurations(AllOtherPluginsConfiguration()
                                    .configuration(mapOf("custom" to mapOf("isolated.docker" to
                                            mapOf("image" to "docker.atl-paas.net/buildeng/agent-buildpipeline",
                                                    "imageSize" to "SMALL",
                                                    "enabled" to "true")))))
                            requirement("system.git.executable")
                            requirement("os", Requirement.MatchType.EQUALS, "Linux")
                            task(::VcsCheckoutTask, "checkout Puppet repository") {
                                checkoutDefault()
                            }
                            task(::ScriptTask, "puppet validate and lint") {
                                inlineBody("""
                        |#!/bin/sh
                        |./utils/ci/sanitychecks
                        |""".trimMargin())
                            }
                            artifact("Puppet deployment", pattern = "tmp/*.tar.bz2")
                            artifact("lint output", pattern = "*.out")
                        }
                        job("PKGCREATE", "Create puppet deployment package") {
                            pluginConfigurations(AllOtherPluginsConfiguration()
                                    .configuration(mapOf("custom" to mapOf("isolated.docker" to
                                            mapOf("image" to "docker.atl-paas.net/buildeng/agent-buildpipeline",
                                                    "imageSize" to "SMALL",
                                                    "enabled" to "true")))))
                            requirement("system.git.executable")
                            requirement("os", Requirement.MatchType.EQUALS, "Linux")
                            task(::VcsCheckoutTask, "checkout Puppet repository") {
                                checkoutDefault()
                            }
                            task(::ScriptTask, "create deployment package") {
                                inlineBody("""
                        |#!/bin/sh
                        |./utils/ci/create_puppet_deployment_package.sh -a "PUPPET_DEPLOY-${'$'}{bamboo.buildNumber}"
                        |mv "tmp/PUPPET_DEPLOY-${'$'}{bamboo.buildNumber}.tar.bz2" .
                        |""".trimMargin())
                            }
                            artifact("Puppet deployment", pattern = "*.tar.bz2", shared = true)
                        }
                        job("PACKERVALIDATE", "Packer validation", "Validate the structure of the packer files. Does NOT execute them.") {
                            pluginConfigurations(AllOtherPluginsConfiguration()
                                    .configuration(mapOf("custom" to mapOf("isolated.docker" to
                                            mapOf("image" to "docker.atl-paas.net/buildeng/agent-buildpipeline",
                                                    "imageSize" to "SMALL",
                                                    "enabled" to "true")))))
                            requirement("system.git.executable")
                            requirement("os", Requirement.MatchType.EQUALS, "Linux")
                            task(::VcsCheckoutTask, "checkout Puppet repository") {
                                checkoutDefault()
                            }
                            task(::ScriptTask, "Validate packer template ec2configs/linux-burn.json") {
                                inlineBody("""
                        |#!/bin/sh
                        |# Validating the syntax only due to the complexity to provide the other parameters (e.g. source_ami and user_data_file)
                        |packer validate -syntax-only ec2configs/linux-burn.json || exit ${'$'}?
                        |""".trimMargin())
                                environmentVariables(
                                        "PACKER_VERSION" to "\${bamboo.packer.version}",
                                        "PACKER_URL" to "\${bamboo.packer.url}"
                                )
                            }
                            task(::ScriptTask, "Validate packer template ec2configs/windows-ami-burn.json") {
                                inlineBody("""
                        |#!/bin/sh
                        |# Validating the syntax only due to the complexity to provide the other parameters (e.g. source_ami and user_data_file)
                        |packer validate -syntax-only ec2configs/windows-ami-burn.json || exit ${'$'}?
                        |""".trimMargin())
                                environmentVariables(
                                        "PACKER_VERSION" to "\${bamboo.packer.version}",
                                        "PACKER_URL" to "\${bamboo.packer.url}"
                                )
                            }
                        }
                        job("PYVALIDATELINT", "Python lint and test") {
                            pluginConfigurations(AllOtherPluginsConfiguration()
                                    .configuration(mapOf("custom" to mapOf("isolated.docker" to
                                            mapOf("image" to "docker.atl-paas.net/buildeng/agent-buildpipeline",
                                                    "imageSize" to "SMALL",
                                                    "enabled" to "true")))))
                            requirement("system.git.executable")
                            requirement("os", Requirement.MatchType.EQUALS, "Linux")
                            task(::VcsCheckoutTask, "checkout Puppet repository") {
                                checkoutDefault()
                            }
                            task(::ScriptTask, "python validate and lint") {
                                inlineBody("""
                        |#!/bin/sh
                        |./utils/ci/python-validate-lint
                        |""".trimMargin())
                            }
                        }
                    }
                    stage("vagrant provision") {
                        fun burnAmi(id: Int, ami: String, os: OsType) = job("AMIBURN${String.format("%02d", id)}", "AMI Burn - Batch ${id} - ${ami}") {
                            pluginConfigurations(AllOtherPluginsConfiguration()
                                    .configuration(mapOf("custom" to mapOf("isolated.docker" to
                                            mapOf("image" to "docker.atl-paas.net/buildeng/agent-buildpipeline",
                                                    "imageSize" to "SMALL",
                                                    "enabled" to "true")))))
                            requirement("os", Requirement.MatchType.EQUALS, "Linux")
                            artifactSubscription("Puppet deployment")
                            task(::ScriptTask, "reuse artifact from previous stage") {
                                inlineBody("""
                        |#!/bin/sh
                        |tar -xjf PUPPET_DEPLOY-${'$'}{bamboo.buildNumber}.tar.bz2
                        |""".trimMargin())
                            }
                            task(::ScriptTask, "get magic files") {
                                interpreter(Interpreter.BINSH_OR_CMDEXE)
                                inlineBody("""
                        |#!/bin/bash
                        |set -e
                        |rsync -e "ssh -p 2222 -o StrictHostKeyChecking=no" -avi --delete --exclude=.git buildeng-puppet-ci@puppet.buildeng.atlassian.com:/data/puppet/magic-files/ magic-files
                        |""".trimMargin())
                            }
                            task(::ScriptTask, "AMI Burn") {
                                val script = when (os) {
                                    OsType.LINUX -> "linux-ami-burn.sh"
                                    OsType.WINDOWS -> "windows-ami-burn.sh"
                                }

                                interpreter(Interpreter.BINSH_OR_CMDEXE)
                                inlineBody("""
                        |#!/bin/sh
                        |set -e
                        |
                        |# Can't use assume role to burn images as it takes more than one hour and the STS roles are only valid for one hour
                        |#temp_role=${'$'}(aws sts assume-role --role-arn "arn:aws:iam::960714566901:role/ami-builder" --role-session-name "ami-burn")
                        |#export AWS_ACCESS_KEY_ID="${'$'}(echo ${'$'}{temp_role} | jq -r .Credentials.AccessKeyId)"
                        |#export AWS_SECRET_ACCESS_KEY="${'$'}(echo ${'$'}{temp_role} | jq -r .Credentials.SecretAccessKey)"
                        |#export AWS_SESSION_TOKEN="${'$'}(echo ${'$'}{temp_role} | jq -r .Credentials.SessionToken)"
                        |export AWS_SECRET_ACCESS_KEY=${'$'}{bamboo.ami.burn.secret.key.password}
                        |export AWS_ACCESS_KEY_ID=${'$'}{bamboo.ami.burn.access.key}
                        |
                        |export PACKER_LOG=1
                        |export PACKER_VERSION=${'$'}{bamboo.packer.version}
                        |export PACKER_URL=${'$'}{bamboo.packer.url}
                        |
                        |./ec2configs/$script "${'$'}{bamboo.buildKey}-${'$'}{bamboo.buildNumber}-${'$'}(cat revision)" $ami ${'$'}{bamboo.ami.burn.delta}
                        |""".trimMargin())
                            }
                            finalTask(::ScriptTask, "purge magic files") {
                                interpreter(Interpreter.BINSH_OR_CMDEXE)
                                inlineBody("""
                         |#!/bin/bash
                         |m -rfv magic-files
                         |""".trimMargin())
                            }
                            artifact("ami-burn-boxes-$id", pattern = "**/ami-burn-$ami.box", shared = true)
                            artifact("ami-burn-capabilities-$id", pattern = "**/bamboo-capabilities*", shared = true)
                            artifact("packer crash logs", pattern = "**/crash.log")
                            artifact("ami-burn-logs", pattern = "**/packer*.txt")
                        }

                        listOf("ec2-ubuntu-deployment-deprecated" to OsType.LINUX,
                                "ec2-ubuntu-deployment" to OsType.LINUX,
                                "ec2-ubuntu-deprecated" to OsType.LINUX,
                                "ec2-ubuntu" to OsType.LINUX,
                                "ec2-ubuntu-oracle-12c-deprecated" to OsType.LINUX,
                                "ec2-ubuntu-oracle-12c" to OsType.LINUX,
                                "ec2-windows-msvc-deployment" to OsType.WINDOWS,
                                "ec2-windows-sql-2008" to OsType.WINDOWS,
                                "ec2-windows-sql-2008-r2" to OsType.WINDOWS,
                                "ec2-windows-sql-2012" to OsType.WINDOWS,
                                "ec2-windows-sql-2014" to OsType.WINDOWS,
                                "ec2-windows-ie10" to OsType.WINDOWS,
                                "ec2-windows-ie11" to OsType.WINDOWS,
                                "ec2-windows-msvc" to OsType.WINDOWS,
                                "ec2-windows-activedirectory" to OsType.WINDOWS)
                                .forEachIndexed { index, pair -> burnAmi(index, pair.first, pair.second) }

                        job("BASEINFRA3", "Base Infra - puppet") {
                            pluginConfigurations(AllOtherPluginsConfiguration()
                                    .configuration(mapOf("custom" to mapOf("isolated.docker" to
                                            mapOf("image" to "docker.atl-paas.net/buildeng/agent-buildpipeline",
                                                    "imageSize" to "SMALL",
                                                    "enabled" to "true")))))
                            requirement("os", Requirement.MatchType.EQUALS, "Linux")
                            artifactSubscription("Puppet deployment")
                            task(::ScriptTask, "reuse artifact from previous stage") {
                                inlineBody("""
                        |#!/bin/sh
                        |tar -xjf PUPPET_DEPLOY-${'$'}{bamboo.buildNumber}.tar.bz2
                        |""".trimMargin())
                            }
                            task(::ScriptTask, "provision puppet from scratch") {
                                fileFromPath("utils/ci/vagrant-provision-from-scratch-aws")
                                argument("puppet --no-parallel")
                            }
                            finalTask(::ScriptTask, "Download test reports from vagrant boxes") {
                                fileFromPath("utils/ci/vagrant-download-test-reports.sh")
                                argument("puppet")
                            }
                            finalTask(TestParserTask::createJUnitParserTask, "Parse cucumber test results") {
                                resultDirectories("test-reports/**/*.xml")
                            }
                            artifact("provision-output", "provision-output", "*")
                            artifact("test-reports", "test-reports", "**/*.xml")
                        }
                        job("BUILDINFRA4", "Build Infra - bambooserver_rds bambooserver") {
                            pluginConfigurations(AllOtherPluginsConfiguration()
                                    .configuration(mapOf("custom" to mapOf("isolated.docker" to
                                            mapOf("image" to "docker.atl-paas.net/buildeng/agent-buildpipeline",
                                                    "imageSize" to "SMALL",
                                                    "enabled" to "true")))))
                            requirement("os", Requirement.MatchType.EQUALS, "Linux")
                            artifactSubscription("Puppet deployment")
                            task(::ScriptTask, "reuse artifact from previous stage") {
                                inlineBody("""
                        |#!/bin/sh
                        |tar -xjf PUPPET_DEPLOY-${'$'}{bamboo.buildNumber}.tar.bz2
                        |""".trimMargin())
                            }
                            task(::ScriptTask, "provision bambooserver_rds bambooserver from scratch") {
                                fileFromPath("utils/ci/vagrant-provision-from-scratch-aws")
                                argument("bambooserver_rds bambooserver --no-parallel")
                            }
                            finalTask(::ScriptTask, "Download test reports from vagrant boxes") {
                                fileFromPath("utils/ci/vagrant-download-test-reports.sh")
                                argument("bambooserver_rds bambooserver")
                            }
                            finalTask(TestParserTask::createJUnitParserTask, "Parse cucumber test results") {
                                resultDirectories("test-reports/**/*.xml")
                            }
                        }
                    }
                    notification(::PlanCompletedNotification) {
                        recipient(::HipChatRecipient) {
                            apiToken("")
                            room("Build Engineering - Private")
                        }
                        recipient("com.atlassian.bamboo.plugins.bamboo-stash-plugin:recipient.stash")
                        recipient("com.atlassian.bamboo.plugins.bamboo-stride-plugin:recipient.stride") {
                            recipientString("Build Engineering - Private")
                        }
                    }
                }
            }
        }

        val bambooServer = BambooServer("https://staging-bamboo.internal.atlassian.com")
        bambooServer.publish(Specs)
    }
}

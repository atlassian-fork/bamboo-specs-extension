package com.atlassian.bamboo.specs.extension

import com.atlassian.bamboo.specs.api.builders.plan.Job
import com.atlassian.bamboo.specs.api.builders.plan.Stage

fun Stage.job(key: String, name: String, description: String? = null, enabled: Boolean? = null, init: SpecsDsl<Job> = {}) {
    val job = Job(name, key)
    if (description != null)
        job.description(description)
    if (enabled != null)
        job.enabled(enabled)
    job.init()
    this.jobs(job)
}

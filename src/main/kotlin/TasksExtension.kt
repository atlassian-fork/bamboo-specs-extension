package com.atlassian.bamboo.specs.extension

import com.atlassian.bamboo.specs.api.builders.AtlassianModule
import com.atlassian.bamboo.specs.api.builders.task.AnyTask
import com.atlassian.bamboo.specs.api.builders.task.Task
import com.atlassian.bamboo.specs.builders.task.*

fun VcsCheckoutTask.checkout(name: String, path: String? = null): VcsCheckoutTask {
    val checkoutItem = CheckoutItem()
    checkoutItem.repository(name)
    if (path != null)
        checkoutItem.path(path)
    this.checkoutItems(checkoutItem)
    return this
}

fun VcsCheckoutTask.checkoutDefault(path: String? = null): VcsCheckoutTask {
    val checkoutItem = CheckoutItem()
    checkoutItem.defaultRepository()
    if (path != null)
        checkoutItem.path(path)
    this.checkoutItems(checkoutItem)
    return this
}

fun ScriptTask.environmentVariables(vararg variables: Pair<String, String>): ScriptTask = environmentVariables(variables.toMap())

fun ScriptTask.environmentVariables(variables: Map<String, String>): ScriptTask =
        environmentVariables(variables.map { "${it.key}=${it.value}" }.joinToString(" "))

fun ArtifactDownloaderTask.allArtifacts(path: String? = null) {
    val downloadItem = DownloadItem()
    downloadItem.allArtifacts(true)
    if (path != null)
        downloadItem.path(path)
    this.artifacts(downloadItem)
}

fun ArtifactDownloaderTask.artifact(name: String, path: String? = null) {
    val downloadItem = DownloadItem()
    downloadItem.artifact(name)
    if (path != null)
        downloadItem.path(path)
    this.artifacts(downloadItem)
}

internal fun <T : Task<in T, *>> taskInitialisation(taskConstructor: () -> T, description: String? = null, enabled: Boolean? = null, init: SpecsDsl<T> = {}): T {
    val task = taskConstructor()
    if (description != null)
        task.description(description)
    if (enabled != null)
        task.enabled(enabled)
    task.init()
    return task
}

internal fun taskInitialisation(taskType: String, description: String? = null, enabled: Boolean? = null, init: SpecsDsl<AnyTask> = {}): AnyTask =
        taskInitialisation({ AnyTask(AtlassianModule(taskType)) }, description, enabled, init)

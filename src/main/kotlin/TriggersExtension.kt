package com.atlassian.bamboo.specs.extension

import com.atlassian.bamboo.specs.api.builders.AtlassianModule
import com.atlassian.bamboo.specs.api.builders.trigger.AnyTrigger
import com.atlassian.bamboo.specs.api.builders.trigger.Trigger

internal fun <T : Trigger<in T, *>> triggerInitialisation(triggerConstructor: () -> T, description: String? = null, enabled: Boolean? = null, init: SpecsDsl<T> = {}): T {
    val trigger = triggerConstructor()
    if (description != null)
        trigger.description(description)
    if (enabled != null)
        trigger.enabled(enabled)
    trigger.init()
    return trigger
}

internal fun triggerInitialisation(triggerType: String, description: String? = null, enabled: Boolean? = null, init: SpecsDsl<AnyTrigger> = {}): AnyTrigger =
        triggerInitialisation({ AnyTrigger(AtlassianModule(triggerType)) }, description, enabled, init)

package com.atlassian.bamboo.specs.extension

import com.atlassian.bamboo.specs.api.builders.plan.Job
import com.atlassian.bamboo.specs.api.builders.plan.artifact.Artifact
import com.atlassian.bamboo.specs.api.builders.plan.artifact.ArtifactSubscription
import com.atlassian.bamboo.specs.api.builders.requirement.Requirement
import com.atlassian.bamboo.specs.api.builders.task.AnyTask
import com.atlassian.bamboo.specs.api.builders.task.Task

fun Job.requirement(key: String, matchType: Requirement.MatchType? = null, value: String? = null, init: SpecsDsl<Requirement> = {}) {
    val requirement = Requirement(key)
    if (matchType != null)
        requirement.matchType(matchType)
    if (value != null)
        requirement.matchValue(value)
    requirement.init()
    this.requirements(requirement)
}

fun Job.artifactSubscription(name: String, destination: String? = null, init: SpecsDsl<ArtifactSubscription> = {}) {
    val subscription = ArtifactSubscription()
    subscription.artifact(name)
    if (destination != null)
        subscription.destination(destination)
    subscription.init()
    this.artifactSubscriptions(subscription)
}

fun Job.artifact(name: String, location: String? = null, pattern: String? = null, shared: Boolean? = null, required: Boolean? = null, init: SpecsDsl<Artifact> = {}) {
    val artifact = Artifact().name(name)
    if (location != null)
        artifact.location(location)
    if (pattern != null)
        artifact.copyPattern(pattern)
    if (shared != null)
        artifact.shared(shared)
    if(required != null)
        artifact.required(required)
    artifact.init()
    this.artifacts(artifact)
}

fun <T : Task<in T, *>> Job.task(taskConstructor: () -> T, description: String? = null, enabled: Boolean? = null, init: SpecsDsl<T> = {}) {
    val task = taskInitialisation(taskConstructor, description, enabled, init)
    this.tasks(task)
}

fun Job.task(taskType: String, description: String? = null, enabled: Boolean? = null, init: SpecsDsl<AnyTask> = {}) {
    val task = taskInitialisation(taskType, description, enabled, init)
    this.tasks(task)
}

fun <T : Task<in T, *>> Job.finalTask(taskConstructor: () -> T, description: String? = null, enabled: Boolean? = null, init: SpecsDsl<T> = {}) {
    val task = taskInitialisation(taskConstructor, description, enabled, init)
    this.finalTasks(task)
}

fun Job.finalTask(taskType: String, description: String? = null, enabled: Boolean? = null, init: SpecsDsl<AnyTask> = {}) {
    val task = taskInitialisation(taskType, description, enabled, init)
    this.finalTasks(task)
}

package com.atlassian.bamboo.specs.extension

import com.atlassian.bamboo.specs.api.builders.AtlassianModule
import com.atlassian.bamboo.specs.api.builders.notification.*

fun <T : NotificationRecipient<in T, *>> Notification.recipient(recipientTypeConstructor: () -> T, init: SpecsDsl<T> = {}) {
    val recipient = recipientTypeConstructor()
    recipient.init()
    this.recipients(recipient)
}

fun Notification.recipient(recipientType: String, init: SpecsDsl<AnyNotificationRecipient> = {}) =
        recipient({ AnyNotificationRecipient(AtlassianModule(recipientType)) }, init)

internal fun <T : NotificationType<in T, *>> notificationInitialisation(notificationTypeConstructor: () -> T, init: SpecsDsl<Notification> = {}): Notification {
    val notification = Notification()
    notification.type(notificationTypeConstructor())
    notification.init()
    return notification
}

internal fun notificationInitialisation(notificationTypeType: String, init: SpecsDsl<Notification> = {}): Notification =
        notificationInitialisation({ AnyNotificationType(AtlassianModule(notificationTypeType)) }, init)

import groovy.xml.dom.DOMCategory.appendNode
import org.gradle.internal.impldep.com.amazonaws.util.XpathUtils.asNode
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import org.jetbrains.dokka.gradle.DokkaTask

plugins {
    kotlin("jvm") version "1.2.41"
    id("nebula.release") version "6.0.0"
    id("org.jetbrains.dokka") version "0.9.17"
    `signing`
    `maven-publish`
}

group = "com.atlassian.bamboo.specs.extension"
val specsVersion = "6.6.0-rc1"

repositories {
    jcenter()
    mavenCentral()
}


dependencies {
    compile(kotlin("stdlib"))
    compile("com.atlassian.bamboo", "bamboo-specs-api", specsVersion)
    compile("com.atlassian.bamboo", "bamboo-specs", specsVersion)
}

tasks {
    withType<KotlinCompile> {
        kotlinOptions {
            jvmTarget = "1.8"
        }
    }

}

val dokka by tasks.getting(DokkaTask::class){
    outputFormat = "javadoc"
    outputDirectory = "$buildDir/javadoc"
}

val javadocJar by tasks.creating(Jar::class) {
    classifier = "javadoc"
    from("$buildDir/javadoc")
    dependsOn(dokka)
}

val sourcesJar by tasks.creating(Jar::class) {
    classifier = "sources"
    from(java.sourceSets["main"].allSource)
}

val pacusername: String? by project
val pacpassword: String? by project
publishing {
    repositories {
        maven {
            url = uri("https://packages.atlassian.com/maven/central")
            credentials {
                username = pacusername
                password = pacpassword
            }
        }
    }
    (publications) {
        "mavenJava"(MavenPublication::class) {
            from(components["java"])
            artifact(sourcesJar)
            artifact(javadocJar)

            pom {
                withXml {
                    asNode().appendNode("name", "Bamboo Specs Extension")
                    asNode().appendNode("description", "A Kotlin DSL for Bamboo Specs")
                    asNode().appendNode("url", "https://bitbucket.org/atlassian/bamboo-specs-extension/src")
                    val licenses = asNode().appendNode("licenses")
                    val license = licenses.appendNode("license")
                    license.appendNode("name", "Atlassian Customer Agreement")
                    license.appendNode("url", "https://www.atlassian.com/legal/customer-agreement")
                    val scm = asNode().appendNode("scm")
                    scm.appendNode("connection", "scm:git:ssh://git@bitbucket.org/atlassian/bamboo-specs-extension.git")
                    scm.appendNode("developerConnection", "scm:git:ssh://git@bitbucket.org/atlassian/bamboo-specs-extension.git")
                    scm.appendNode("url", "https://bitbucket.org/atlassian/bamboo-specs-extension/src")
                    val developers = asNode().appendNode("developers")
                    val developer = developers.appendNode("developer")
                    developer.appendNode("name", "Charlie")
                    developer.appendNode("email", "devrel@atlassian.com")
                    developer.appendNode("organization", "Atlassian")
                    developer.appendNode("organizationUrl", "https://atlassian.com")
                }
            }
        }
    }
}


signing {
    sign(publishing.publications["mavenJava"])
}
